<?php

namespace Turahe\Filepond;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class FilepondServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerRoutes();
    }


    /**
     * Register Filepond routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group([
            'prefix' => 'api/v1',
            'middleware' => 'api',
        ], function () {
            $this->loadRoutesFrom(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'routes'.DIRECTORY_SEPARATOR.'web.php');
        });
    }
}
