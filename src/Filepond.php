<?php

namespace Turahe\Filepond;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Turahe\Filepond\Exceptions\InvalidPathException;

class Filepond
{
    /**
     * Converts the given path into a filepond server id.
     *
     * @param  string $path
     *
     * @return string
     */
    public function getServerIdFromPath(string $path): string
    {
        return Crypt::encryptString($path);
    }

    /**
     * Converts the given filepond server id into a path.
     *
     * @param  string $serverId
     *
     * @return string
     */
    public function getPathFromServerId(string $serverId): string
    {
        if (! trim($serverId)) {
            throw new InvalidPathException();
        }

        $filePath = Crypt::decryptString($serverId);
//        if (! Str::startsWith($filePath, $this->getBasePath())) {
//            throw new InvalidPathException();
//        }

        return $filePath;
    }

    /**
     * Get the storage base path for files.
     *
     * @return string
     */
    public function getBasePath(): string
    {
        return Storage::disk('local')
            ->path('tmp');
    }
}
